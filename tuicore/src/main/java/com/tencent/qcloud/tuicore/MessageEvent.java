package com.tencent.qcloud.tuicore;

/**
 * @ProjectName :TimKitUi
 * @Author : QI.JIA
 * @Time : 2021/11/23 10:44
 * @Description :
 */
public class MessageEvent {
    public String type;
    public String content;
    public static final String REPORT_MEMBER="REPORT_MEMBER";
    public static final String REPORT_GROUP="REPORT_GROUP";

    public MessageEvent(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
